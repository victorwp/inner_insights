// App.js
import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./screens/HomeScreen";
import AddEntryScreen from "./screens/AddEntryScreen";
import MoodGraphScreen from "./screens/MoodGraphScreen";
import EntryContext from "./EntryContext";

const Stack = createStackNavigator();

export default function App() {
  const [entries, setEntries] = useState([]);

  const addEntry = (entry) => {
    setEntries([...entries, entry]);
  };

  return (
    <EntryContext.Provider value={{ entries, addEntry }}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ title: "Journal" }}
          />
          <Stack.Screen
            name="AddEntry"
            component={AddEntryScreen}
            options={{ title: "Add Entry" }}
          />
          <Stack.Screen
            name="MoodGraph"
            component={MoodGraphScreen}
            options={{ title: "Mood Graph" }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </EntryContext.Provider>
  );
}
