import React, { useContext } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import styles from "../styles";
import EntryContext from "../EntryContext";

const HomeScreen = ({ navigation }) => {
  const { entries } = useContext(EntryContext);

  return (
    <View style={styles.container}>
      <FlatList
        data={entries}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text style={styles.entryText}>{item.text}</Text>
            <Text style={styles.moodText}>{item.mood}</Text>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
      <TouchableOpacity
        style={styles.addButton}
        onPress={() => navigation.navigate("AddEntry")}
      >
        <Ionicons name="add" size={30} color="white" />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.graphButton}
        onPress={() => navigation.navigate("MoodGraph")}
      >
        <Ionicons name="git-branch" size={30} color="white" />
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;
