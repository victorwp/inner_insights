import React, { useState, useContext } from "react";
import { View, TextInput, Button, Text, TouchableOpacity } from "react-native";
import styles from "../styles";
import EntryContext from "../EntryContext";

const AddEntryScreen = ({ navigation }) => {
  const [text, setText] = useState("");
  const [mood, setMood] = useState(3);
  const { addEntry } = useContext(EntryContext);

  const handleSubmit = () => {
    if (text.trim() === "") return;

    addEntry({
      text,
      mood,
      date: new Date().toISOString(),
    });

    setText("");
    setMood(3);
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        multiline
        numberOfLines={4}
        placeholder="What's on your mind?"
        value={text}
        onChangeText={setText}
      />
      <Text style={styles.moodLabel}>Mood: {mood}</Text>
      <View style={styles.moodButtonsContainer}>
        {[1, 2, 3, 4, 5].map((num) => (
          <TouchableOpacity
            key={num}
            style={mood === num ? styles.selectedMoodButton : styles.moodButton}
            onPress={() => setMood(num)}
          >
            <Text style={styles.moodButtonText}>{num}</Text>
          </TouchableOpacity>
        ))}
      </View>
      <Button title="Submit" onPress={handleSubmit} />
    </View>
  );
};

export default AddEntryScreen;
