import React, { useState, useContext } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { LineChart, Grid } from "react-native-svg-charts";
import * as shape from "d3-shape";
import styles from "../styles";
import EntryContext from "../EntryContext";

const MoodGraphScreen = () => {
  const { entries } = useContext(EntryContext);
  const [selectedTimeRange, setSelectedTimeRange] = useState("7days");

  const filterEntries = () => {
    const now = new Date();
    let filteredEntries;

    switch (selectedTimeRange) {
      case "7days":
        filteredEntries = entries.filter(
          (entry) => now - new Date(entry.date) <= 7 * 24 * 60 * 60 * 1000
        );
        break;
      case "1month":
        filteredEntries = entries.filter(
          (entry) => now - new Date(entry.date) <= 30 * 24 * 60 * 60 * 1000
        );
        break;
      case "1year":
        filteredEntries = entries.filter(
          (entry) => now - new Date(entry.date) <= 365 * 24 * 60 * 60 * 1000
        );
        break;
      default:
        filteredEntries = entries;
    }

    return filteredEntries.map((entry) => entry.mood);
  };

  const renderButton = (label, value) => {
    const isSelected = selectedTimeRange === value;
    return (
      <TouchableOpacity
        style={isSelected ? styles.selectedRangeButton : styles.rangeButton}
        onPress={() => setSelectedTimeRange(value)}
      >
        <Text style={isSelected ? styles.selectedRangeText : styles.rangeText}>
          {label}
        </Text>
      </TouchableOpacity>
    );
  };

  const moodData = filterEntries();

  return (
    <View style={styles.container}>
      <Text style={styles.graphTitle}>Mood Graph</Text>
      <View style={styles.rangeButtonsContainer}>
        {renderButton("7 days", "7days")}
        {renderButton("1 month", "1month")}
        {renderButton("1 year", "1year")}
        {renderButton("All time", "all")}
      </View>
      <LineChart
        style={{ height: 200, width: "100%" }}
        data={moodData}
        svg={{ stroke: "rgb(134, 65, 244)" }}
        contentInset={{ top: 20, bottom: 20 }}
        curve={shape.curveNatural}
        gridMin={1}
        gridMax={5}
      >
        <Grid />
      </LineChart>
    </View>
  );
};

export default MoodGraphScreen;
