import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 10,
  },
  input: {
    borderWidth: 1,
    borderColor: "gray",
    padding: 10,
    marginBottom: 10,
    fontSize: 16,
  },
  addButton: {
    alignItems: "center",
    justifyContent: "center",
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: "blue",
    position: "absolute",
    bottom: 20,
    right: 20,
  },
  listItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    padding: 10,
  },
  entryText: {
    fontSize: 16,
  },
  moodText: {
    fontSize: 16,
    fontWeight: "bold",
  },
  moodContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  moodLabel: {
    fontSize: 16,
    marginRight: 10,
  },
  mood: {
    fontSize: 16,
    padding: 5,
    marginRight: 5,
  },
  selectedMood: {
    fontSize: 16,
    padding: 5,
    marginRight: 5,
    backgroundColor: "blue",
    color: "white",
  },
  rangeButtonsContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 10,
  },
  rangeButton: {
    padding: 10,
    borderRadius: 5,
  },
  selectedRangeButton: {
    backgroundColor: "blue",
    padding: 10,
    borderRadius: 5,
  },
  rangeText: {
    fontSize: 16,
  },
});

export default styles;
