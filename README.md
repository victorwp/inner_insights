# Inner insights

## About the project

**Inner insights** is an innovative **React native** app, developed by me.
It is a **journaling app** that helps you to **track your mood** and **write down your thoughts**.
Backed by science and psychology, it helps you to **understand yourself better** and **improve your mental health**.


## Features

- **Journal**: A place to write down your thoughts and feelings.
- **Mood graph**: A graph that shows your mood over time.


## Installation

To run the app locally, follow these steps:

## 1. Clone the repository:

```shell
git clone https://gitlab.com/victorwp/inner_insights
cd inner_insights
```

## 2. Install the dependencies:

```shell
npm expo tart
```


## 3. Start a local development server on port 3000:

```shell
npm 
```

After running the app, download and open the **Expo Go** app on your phone. Scan the QR code in the terminal to open the app.

## License

This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for more details.